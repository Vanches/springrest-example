-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 09 2016 г., 10:33
-- Версия сервера: 5.5.45
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `Example`
--

-- --------------------------------------------------------

--
-- Структура таблицы `IMAGE`
--

CREATE TABLE IF NOT EXISTS `IMAGE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `IMAGE`
--

INSERT INTO `IMAGE` (`id`, `data`) VALUES
(1, 'test'),
(2, 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `USERS`
--

CREATE TABLE IF NOT EXISTS `USERS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IMAGE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IMAGE_ID` (`IMAGE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `USERS`
--

INSERT INTO `USERS` (`ID`, `IMAGE_ID`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `USER_ROLE`
--

CREATE TABLE IF NOT EXISTS `USER_ROLE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `USER_ROLE`
--

INSERT INTO `USER_ROLE` (`ID`, `TYPE`) VALUES
(1, 'CAT'),
(2, 'DOG');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
