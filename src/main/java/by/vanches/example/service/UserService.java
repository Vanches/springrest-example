package by.vanches.example.service;

import by.vanches.example.model.User;

import java.util.List;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 1:29
 */

public interface UserService {

    List<User> getAllUsers();

    User getUserById(Integer userId);

    void deleteUser(Integer userId);

    void createUser(User user);
}
