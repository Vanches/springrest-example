package by.vanches.example.service.impl;

import by.vanches.example.model.User;
import by.vanches.example.repository.UserRepository;
import by.vanches.example.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 1:29
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(Integer userId) {
        return userRepository.findOne(userId);
    }

    public void deleteUser(Integer userId) {
        userRepository.delete(userId);
    }

    public void createUser(User user) {
        userRepository.save(user);
    }
}
