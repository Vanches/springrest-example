package by.vanches.example.repository;

import by.vanches.example.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 1:28
 */

public interface UserRepository extends JpaRepository<User, Integer> {
}
