package by.vanches.example.model;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 10:01
 */

public enum Role {
    ADMIN, MODERATOR, USER, CAT, DOG;
}
