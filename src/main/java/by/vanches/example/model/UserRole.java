package by.vanches.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.persistence.Table;
import javax.persistence.*;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 10:01
 */
@Entity
@Table(name = "USER_ROLE")
public class UserRole {

    @Id
    @Column(name="ID", unique=true, nullable=false)
    @GeneratedValue(generator="gen")
    @GenericGenerator(name="gen", strategy="foreign", parameters=@Parameter(name="property", value="user"))
    private Integer id;

    @Column(name="TYPE")
    @Enumerated(EnumType.STRING)
    private Role role;

    @JsonIgnore
    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
