package by.vanches.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 0:14
 */
@Entity
@Table(name = "IMAGE")
public class Image {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "data", length = 128)
    private String data;

    @JsonIgnore
    @OneToMany(mappedBy = "image", cascade = CascadeType.ALL)
    private Set<User> userSet=new HashSet<User>(0);;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Set<User> getUserSet() {
        return userSet;
    }

    public void setUserSet(Set<User> userSet) {
        this.userSet = userSet;
    }
}
