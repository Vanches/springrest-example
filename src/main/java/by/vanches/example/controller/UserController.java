package by.vanches.example.controller;

import by.vanches.example.model.Image;
import by.vanches.example.model.Role;
import by.vanches.example.model.User;
import by.vanches.example.model.UserRole;
import by.vanches.example.model.dto.UserDto;
import by.vanches.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 0:59
 */
@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    private UserService userService;

    private static Logger LOGGER = Logger.getLogger(UserController.class.getName());

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getNumbers() {
        return new ResponseEntity<List<User>>(userService.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("userId") Integer userId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            LOGGER.info("User with id " + userId + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody UserDto user, UriComponentsBuilder ucBuilder) {
        LOGGER.info("Creating User ");
        User tempUser = new User();

        //TODO - Проверить, существует ли такой пользователь в БД
        /*
        if(){
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        */

        //TODO - Тут значения создаются новые. В идеале, их нужно сохранять ДО и по id-шке вынимать и просетывать (Это касается OneToMany).

        UserRole userRole = new UserRole();
        userRole.setRole(Role.USER);
        tempUser.setUserRole(userRole);

        Image image = new Image();
        image.setData("");

        tempUser.setImage(image);

        Set<User> imageSet = new HashSet<User>();
        imageSet.add(tempUser);


        //imageRepository.save(imageSet)
        //В теории, должно все просетаться и записаться. Не тетсил, не могу быть увернным

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("userId") Integer userId) {

        LOGGER.info("Delete User with id " + userId);
        User user = userService.getUserById(userId);

        if (user == null) {
            LOGGER.info("User with id " + userId + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.deleteUser(userId);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
}
