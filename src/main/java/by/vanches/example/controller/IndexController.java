package by.vanches.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Vanja Novak
 * Date: 09.04.2016
 * Time: 0:15
 */

@RestController
public class IndexController {

    public final static String[] NUMBERS = {"one", "two", "three", "four", "five", "six"};

    @RequestMapping(value = "/index", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String[]> getNumbers() {
        return new ResponseEntity<String[]>(NUMBERS, HttpStatus.OK);
    }
}
